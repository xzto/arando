TOP     = $(shell pwd)
BUILDDIR ?= build
# TARGET_NAME = kmain

# ---

# Define these in the other makefiles
infiles-y :=

# Include other makefiles
include src/Makefile

build-obj := $(addprefix $(BUILDDIR)/, $(addsuffix .o, $(infiles-y)))
deps := $(addprefix $(BUILDDIR)/, $(addsuffix .o.dep, $(infiles-y)))
-include $(deps)

# ----

# NOTE: With `VAR := value`, the environment's VAR will be ignored however if
# VAR=xxx is provided in the cmdline that value will be used instead.
Q = @
cross-prefix = i686-elf-
OBJDUMP = $(cross-prefix)objdump
OBJCOPY = $(cross-prefix)objcopy
CC = $(cross-prefix)gcc
MY_LIBGCC = -lgcc
# CC := zig cc --target=i386-freestanding-none
CCLD = $(CC)
NASM = yasm
CFLAGS =
CCLDFLAGS =
NASMFLAGS =
# config-local.mk is a file that is ignored by git. I will replace CC and CCLD there with ccache.
-include config-local.mk

# -nostdinc -fno-builtin
		 # -nostartfiles -nodefaultlibs
# -nostdlib -fno-stack-protector
# TODO: grub error 13 with -O0
# TODO: grub error 28: selected item cannot fit into memory with ldflags '-z max-page-size=4'
# optlevel = -Og -ggdb3
optlevel = -Os -ggdb3


KERNEL_STACK_SIZE_BYTES = 32768
MYCFLAGS = -m32                                                                         \
		 -masm=intel                                                                    \
		 -ffreestanding -nostdlib -fno-pic -fno-pie                                     \
		 -flto -ffunction-sections -fdata-sections                                      \
		 -DKERNEL_STACK_SIZE_BYTES=$(KERNEL_STACK_SIZE_BYTES)                           \
		 -mno-red-zone                                                                  \
		 -std=gnu11                                                                     \
		 -Wall -Wextra -Wno-error                                                       \
		 -Wignored-attributes -Wimplicit-fallthrough -Wuninitialized                    \
		 -Winit-self -Wshadow -Wundef -Wfloat-conversion -Wconversion -Wsign-conversion \
		 -Wno-unused-parameter -Wno-unused-function -Wno-unused-variable -Wtype-limits  \
		 -Wwrite-strings -Werror=implicit-function-declaration -Werror=return-type      \
		 -Wunused-result -Wnarrowing                                                    \
		 -Wmissing-field-initializers                                                   \
		 $(optlevel)
MYCCLDFLAGS = \
	$(MYCFLAGS) -T src/arando/kernel/entry/link.ld \
	-Wl,--no-warn-rwx-segments \
	-Wl,-melf_i386 -Wl,--fatal-warnings -Wl,-n -Wl,--sort-section=name -Wl,--gc-sections
# -z max-page-size=4

MYNASMFLAGS = -p nasm -f elf32 -D KERNEL_STACK_SIZE_BYTES=$(KERNEL_STACK_SIZE_BYTES) -g dwarf2

MYCFLAGS += -Isrc/

MYCFLAGS += $(CFLAGS)
MYNASMFLAGS += $(NASMFLAGS)
MYCCLDFLAGS += $(CCLDFLAGS)


KERNEL_ELF = $(BUILDDIR)/kernel.elf


.SUFFIXES:
.SECONDARY: $(BUILDDIR) $(build-obj)
.PHONY: all qemu qemu-stopped clean mbchk show
.DEFAULT_GOAL := all

# ---

all: $(KERNEL_ELF) $(BUILDDIR)/kernel.lss $(BUILDDIR)/kernel.bin Makefile


# Re build everything if CFLAGS or anything changed.
# I make a target called $(cached_vars_file) and everything will depend on it.
# When variables change, the target will be marked as PHONY (always make it)
# therefore all targets that depend on $(cached_vars_file) will also run.
cached_vars_file := $(BUILDDIR)/.cached_vars.txt
$(cached_vars_file): ;

ifeq (rebuild,$(shell echo "$(MYCFLAGS)$(MYCCLDFLAGS)$(MYNASMFLAGS)$(CC)$(CCLD)$(NASM)" | ./scripts/make_cached_vars.sh $(cached_vars_file)))
.PHONY: $(cached_vars_file)
endif



show:
	@echo CC = $(CC)
	@echo NASM = $(NASM)
	@echo CCLD = $(CCLD)
	@echo BUILDDIR = $(BUILDDIR)
	@echo "cflags = $(MYCFLAGS)" | fmt
	@echo "cdldflags = $(MYCCLDFLAGS)" | fmt
	@echo
	@echo "infiles = $(infiles-y)" | fmt
	@echo
	@echo "obj = $(build-obj)" | fmt
	@echo

clean:
	rm -rf build


$(BUILDDIR)/%:
	@echo Shit $@
	@echo This probably means the file was not found or there is no rule for this file.
	@false

$(BUILDDIR)/%.dep:
	@:

$(BUILDDIR)/%.c.o: %.c Makefile $(cached_vars_file)
	@mkdir -p $(dir $@)
	@echo " cc   " $<
	$(Q)$(CC) $(MYCFLAGS) -MD -MP -MT $@ -MF $@.dep -c $< -o $@

$(BUILDDIR)/%.s.o: %.s Makefile $(cached_vars_file)
	@mkdir -p $(dir $@)
	@echo " nasm " $<
	$(Q)$(NASM) $(MYNASMFLAGS) $< -o $@ # -MD $@.dep


mbchk: $(KERNEL_ELF)
	grub-file --is-x86-multiboot $(KERNEL_ELF)

$(KERNEL_ELF): $(build-obj) Makefile $(cached_vars_file)
	@echo " ccld " $@
	$(Q)$(CCLD) $(MYCCLDFLAGS) $(build-obj) $(MY_LIBGCC) -o $@
	$(Q)stat -c '%s	%n' $@

$(BUILDDIR)/%.lss: $(BUILDDIR)/%.elf
	$(Q)$(OBJDUMP) -M intel --headers --source-comment -S -d $< > $@
	$(Q)stat -c '%s	%n' $@

$(BUILDDIR)/%.bin: $(BUILDDIR)/%.elf
	$(Q)$(OBJCOPY) -O binary  $< $@
	$(Q)stat -c '%s	%n' $@

RUN_QEMU = qemu-system-i386 -kernel $(KERNEL_ELF) -gdb tcp::1234 -nographic
qemu: $(KERNEL_ELF)
	@echo "Exit qemu with 'Ctrl-A X'"
	$(RUN_QEMU)
qemu-stopped: $(KERNEL_ELF)
	$(RUN_QEMU) -S



ifeq ($(MAKE_RESTARTS),)
Makefile: $(filter-out Makefile,$(filter-out %.dep,$(MAKEFILE_LIST)))
	@touch $@
endif
