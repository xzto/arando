// _PUBLIC_HEADER_
#pragma once

// #define ATOMIC_RELAXED __ATOMIC_RELAXED
// #define ATOMIC_CONSUME __ATOMIC_CONSUME
// #define ATOMIC_ACQUIRE __ATOMIC_ACQUIRE
// #define ATOMIC_RELEASE __ATOMIC_RELEASE
// #define ATOMIC_ACQ_REL __ATOMIC_ACQ_REL
// #define ATOMIC_SEQ_CST __ATOMIC_SEQ_CST

static const struct {
    int Relaxed;
    int Consume;
    int Acquire;
    int Release;
    int AcqRel;
    int SeqCst;
} AtomicOrder = {
    .Relaxed = __ATOMIC_RELAXED,
    .Consume = __ATOMIC_CONSUME,
    .Acquire = __ATOMIC_ACQUIRE,
    .Release = __ATOMIC_RELEASE,
    .AcqRel  = __ATOMIC_ACQ_REL,
    .SeqCst  = __ATOMIC_SEQ_CST,
};

#define atomic_fetch_add(ptr, x, order) __atomic_fetch_add(ptr, x, order)
#define atomic_fetch_sub(ptr, x, order) __atomic_fetch_sub(ptr, x, order)
#define atomic_load(ptr, order)         __atomic_load_n(ptr, order)
#define atomic_store(ptr, x, order)     __atomic_store_n(ptr, x, order)
#define atomic_xchg(ptr, x, order)      __atomic_exchange_n(ptr, x, order)
#define atomic_compare_exchange(ptr, expected, desired, order_success, order_failure) \
    __atomic_compare_exchange_n(ptr, expected, desired, false, order_success, order_failure)
