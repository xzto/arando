// _PUBLIC_HEADER_
#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

typedef size_t      usize;
typedef ptrdiff_t   isize;


typedef float   f32;
typedef double  f64;

#define static_assert(cond, ...) _Static_assert(cond, "" __VA_ARGS__)

static_assert(sizeof(u8)  == 1);
static_assert(sizeof(u16) == 2);
static_assert(sizeof(u32) == 4);
static_assert(sizeof(u64) == 8);
static_assert(sizeof(i8)  == 1);
static_assert(sizeof(i16) == 2);
static_assert(sizeof(i32) == 4);
static_assert(sizeof(i64) == 8);
static_assert(sizeof(usize) == 4);
static_assert(sizeof(isize) == 4);
static_assert(sizeof(void *) == 4);
static_assert(sizeof(f32) == 4);
static_assert(sizeof(f64) == 8);


#ifndef ASDF_SAFE_SLOW
# define ASDF_SAFE_SLOW 1
#endif



#define JOIN2_A(a, b) a##b
#define JOIN2(a, b) JOIN2_A(a, b)


#define EXPAND_STR_LIT_PTR_LEN(str) (u8 const*)(str), (ARRAY_COUNT(str)-1)
#define EXPAND_STR_LIT_LEN_PTR(str) (int)(ARRAY_COUNT(str)-1), (u8 const*)(str)

#define FIELD_PARENT_POINTER(field_ptr, ParentType, field_name) \
    (ParentType*)((usize)(field_ptr) - (usize)(offsetof(ParentType, field_name)))
#define PACKED __attribute__((__packed__))
#define INLINE __attribute__((__always_inline__))
#define NOINLINE __attribute__((__noinline__))
#define NODISCARD __attribute__((__warn_unused_result__))
#define NORETURN __attribute__((__noreturn__))
#define ARRAY_COUNT(x) (sizeof((x))/sizeof((x)[0]))
#define EXPAND_STR_FOR_PRINTF(s) (int)(s).len, (const char*)(s).data

// TODO: qemu triple faults on int3
#define DEBUG_BREAK() do { asm volatile ("int3"); i32 volatile JOIN2(_debug_break_, __COUNTER__) = 123; } while (false)

#define PANIC(msg) do { asdf_panic_handler(__FILE__, __LINE__, msg); } while (false)
#define PANIC_FMT(fmt, ...) do { asdf_panic_handler_fmt(__FILE__, __LINE__, fmt, ##__VA_ARGS__); } while (false)
#define UNIMPLEMENTED() PANIC("Not implemented")
#if ASDF_SAFE_SLOW
# define UNREACHABLE()              PANIC("Unreachable")
# define UNREACHABLE_FMT(fmt, ...)  PANIC_FMT("Unreachable: " fmt, ##__VA_ARGS__)
#else
# define UNREACHABLE()              do { __builtin_unreachable(); } while (false)
# define UNREACHABLE_FMT(fmt, ...)  do { __builtin_unreachable(); } while (false)
#endif
#define ASSERT(x)               do { if (!(x)) { UNREACHABLE_FMT("Assertion fail: `%s`", #x); } } while (false)
#define ASSERT_FMT(x, fmt, ...) do { if (!(x)) { UNREACHABLE_FMT("Assertion fail: `%s`: " fmt, #x, ##__VA_ARGS__); } } while (false)

NOINLINE NORETURN void asdf_panic_handler_fmt(const char *file, int line, const char *fmt, ...);
NOINLINE NORETURN void asdf_panic_handler(const char *file, int line, const char *str);




static inline bool between(usize x, usize lo, usize hi) {
    ASSERT(lo <= hi);
    return x >= lo && x <= hi;
}
static inline bool abc(usize idx, usize len) {
    return /* idx >= 0 && */ idx < len;
}





// typedef struct {
//     u8 *ptr;
//     usize len;
// } Slice_u8;

// #define mem_zero_slice(slice) do { mem_set((slice).ptr, 0, (slice).len*sizeof(*(slice).ptr)); } while (0)
#define mem_zero(ptr, size) do { mem_set(ptr, 0, size); } while (0)
#define mem_set(ptr, byte_val, size) do { __builtin_memset(ptr, byte_val, size); } while (0)
#define mem_cpy(dst, src, size) do { __builtin_memcpy(dst, src, size); } while (0)
#define mem_move(dst, src, size) do { __builtin_memmove(dst, src, size); } while (0)
#define cstr_len(s) __builtin_strlen(s)
#define cstr_len_max(s, max) __builtin_strnlen(s, max)

#define mem_set_ptr_type_one(ptr, byte_val) do { mem_set(ptr, byte_val, sizeof(*(ptr))); } while (0)
#define mem_set_ptr_type_many(ptr, count, byte_val) do { mem_set(ptr, byte_val, (count)*sizeof(*(ptr))); } while (0)
#define mem_cpy_ptr_type_many(dst, src, count) do {                              \
    static_assert(__builtin_types_compatible_p(typeof(*(dst), typeof(*(src))))); \
    mem_cpy(dst, src, (count)*sizeof(*(src)))                                    \
} while (0)
#define mem_move_ptr_type_many(dst, src, count) do {                             \
    static_assert(__builtin_types_compatible_p(typeof(*(dst), typeof(*(src))))); \
    mem_move(dst, src, (count)*sizeof(*(src)))                                   \
} while (0)

// __GNUC__
#define corelib_swap2(ptr1, ptr2) ({           \
    __auto_type __temp_corelib_swap2 = *(ptr1); \
    *(ptr1) = *(ptr2);                          \
    *(ptr2) = __temp_corelib_swap2;             \
})
