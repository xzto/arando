#include <arando/corelib/basic.h>
#include <arando/kernel/writer/mod.h>
#include <arando/kernel/serial/mod.h>
#include <arando/kernel/framebuffer/mod.h>
#include <arando/kernel/cpu/mod.h>
#include <stdarg.h>

/// Return how many digits were printed
static usize print_num_base10_to_buf(u8 *buf, usize buflen, u32 n) {
    usize i = 0;
    while (true) {
        if (i >= buflen) break;
        u8 nmod10 = (u8)(n % 10);
        buf[i] = nmod10 + '0';
        n /= 10;
        i += 1;
        if (n == 0) break;
    }
    // reverse buf[0..i]
    usize count_printed = i;
    for (i = 0; i < count_printed/2; i++) {
        corelib_swap2(&buf[i], &buf[count_printed-i-1]);
    }
    return count_printed;
}

NOINLINE NORETURN void asdf_panic_handler_fmt(const char *file, int line, const char *fmt, ...) {
    static u32 inside_panic = 0;
    inside_panic += 1;
    if (inside_panic > 1) {
        if (inside_panic == 2) {
            serial_text_write(EXPAND_STR_LIT_PTR_LEN("====Triggered panic inside the panic handler!!!!\n"));
        }
        goto halt;
    }
    serial_text_write(EXPAND_STR_LIT_PTR_LEN("=====Kernel Panic=====\n"));
    TRY_OR_PANIC(writer_printf(serial_text_writer(), "Panic at %s:%d: ", file, line));
    {
        va_list ap;
        va_start(ap, fmt);
        TRY_OR_PANIC(writer_printf_va(serial_text_writer(), fmt, ap));
        va_end(ap);
    }
    serial_text_put_char('\n');
    // *(volatile int *)0 = 0;
    // DEBUG_BREAK(); // TODO qemu triple faults with this
    halt:;
    cpu_halt_forever();
    while (true) {}
    __builtin_unreachable();
}
NOINLINE NORETURN void asdf_panic_handler(const char *file, int line, const char *str) {
    asdf_panic_handler_fmt(file, line, "%s", str);
}

// Don't use these functions directly; use mem_copy, mem_move, mem_set, mem_zero, cstr_len from corelib.h (they are defined to __builtin_*).
usize strlen(u8 const *s) {
    usize result = 0;
    for (; *s != 0; s++) result++;
    return result;
}
usize strnlen(u8 const *s, usize max) {
    usize result = 0;
    for (; *s != 0 && result < max; s++) result++;
    return result;
}
void *memset(void *ptr, int c, usize size) {
    u8 *p = ptr;
    for (usize i = 0; i < size; i++) {
        *p++ = (u8)c;
    }
    return ptr;
}

void memcpy(void *dst, const void *src, usize size) {
    u8 *d = dst;
    u8 const *s = src;
    for (usize i = 0; i < size; i++) {
        *d++ = *s++;
    }
}
void memmove(void *dst, const void *src, usize size) {
    u8 *d = dst;
    u8 const *s = src;
    if (d < s) {
        for (usize i = 0; i < size; i++) {
            *d++ = *s++;
        }
    } else if (d > s) {
        d = dst + size - 1;
        s = src + size - 1;
        for (usize i = 0; i < size; i++) {
            *d-- = *s--;
        }
    }
}
