// _PUBLIC_HEADER_
#pragma once

#include <arando/corelib/basic.h>
#include <stdbool.h>

typedef enum {
    ArandoError_Ok,
    ArandoError_SomethingWentWrong,
    // TODO: More errors
} ArandoError_E;

typedef struct {
    ArandoError_E e;
} ArandoError;

inline static NODISCARD bool arando_is_ok(ArandoError const e) { return e.e == ArandoError_Ok; }

#define TRY(x) do { \
    ArandoError const _tmp_err = (x); if (!arando_is_ok(_tmp_err)) { return _tmp_err; } \
} while (false)

#define TRY_OR_PANIC(x) do { \
    ArandoError const _tmp_err = (x); if (!arando_is_ok(_tmp_err)) { PANIC_FMT("Bad ArandoError: %d", (int)_tmp_err.e); } \
} while (false)
