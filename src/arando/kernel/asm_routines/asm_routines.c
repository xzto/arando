#include <arando/corelib/basic.h>
#include <arando/kernel/asm_routines/mod.h>

void asm_out8(u16 port, u8 data) {
    asm volatile (
        "out %[port], %[data]"
        :: [data]"a"(data), [port]"d"(port)
    );
}

u8 asm_in8(u16 port) {
    u8 result;
    asm volatile (
        "in %[result], %[port]"
        : [result]"=a"(result)
        : [port]"d"(port)
    );
    return result;
}

u16 asm_in16(u16 port) {
    u16 result;
    asm volatile (
        "in %[result], %[port]"
        : [result]"=a"(result)
        : [port]"d"(port)
    );
    return result;
}

u32 asm_in32(u16 port) {
    u32 result;
    asm volatile (
        "in %[result], %[port]"
        : [result]"=a"(result)
        : [port]"d"(port)
    );
    return result;
}

void asm_load_gdt(GdtDescriptor const *gdt) {
    asm volatile (
        "lgdt [%[gdt]]"
        :
        : [gdt]"r"((usize)gdt)
        : "memory"
    );
}

u32 gdt_entry_get_limit(GdtEntry const *entry) {
    u32 result = 0;
    result |= (u32)(entry->limit_0_15)  << 0;
    result |= (u32)(entry->limit_16_19) << 16;
    return result;
}
u32 gdt_entry_get_base(GdtEntry const *entry) {
    u32 result = 0;
    result |= (u32)(entry->base_0_15)  << 0;
    result |= (u32)(entry->base_16_23) << 16;
    result |= (u32)(entry->base_24_31) << 24;
    return result;
}

void gdt_entry_init(GdtEntry *ret_entry, u32 base, u32 limit, u8 access_byte, u8 flags) {
    limit &= 0xfffff;
    *ret_entry = (GdtEntry){
        .limit_0_15 = limit & 0xffff,
        .base_0_15 = base & 0xffff,
        .base_16_23 = (u8)((base & (0x00ff0000)) >> 0x10),
        .access_byte = access_byte,
        .base_24_31 = (u8)((base & 0xff000000) >> 0x08),
        // TODO: Verify that this is not a bug!
        .flags = (u8)(flags & 0xf),
        .limit_16_19 = (u8)((limit & 0xf0000) >> 0x10),
    };
}
