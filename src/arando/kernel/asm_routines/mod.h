// _PUBLIC_HEADER_
#pragma once

#include <arando/corelib/basic.h>

void asm_out8(u16 port, u8 data);
u8  asm_in8(u16 port);
u16 asm_in16(u16 port);
u32 asm_in32(u16 port);

// TODO:
typedef struct {
    u16 limit_0_15;
    u16 base_0_15;
    u8 base_16_23;
    u8 access_byte;
    u8 limit_16_19: 4;
    u8 flags: 4;
    u8 base_24_31;
} PACKED GdtEntry;
static_assert(sizeof(GdtEntry) == 8);

u32 gdt_entry_get_limit(GdtEntry const *entry);
u32 gdt_entry_get_base(GdtEntry const *entry);
void gdt_entry_init(GdtEntry *ret_entry, u32 base, u32 limit, u8 access_byte, u8 flags);


/// At least three entries are needed. The first one is the null entry.
typedef struct {
    /// Linear Address to the first segment descriptor entry.
    usize first_entry_linear_address;
    /// TODO: Is this number of bytes or number of entries?
    /// Byte count of the table subracted by 1. The max 16 bit number is 65535 while the max number of entires in a gdt is 8192 (8 bytes per entry)
    u16 size_in_bytes_minus_one;
} PACKED GdtDescriptor;
void asm_load_gdt(GdtDescriptor const *gdt);
