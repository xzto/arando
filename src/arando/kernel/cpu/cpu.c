#include <arando/corelib/basic.h>
#include <arando/kernel/cpu/mod.h>

// Should this be NORETURN?
void cpu_halt() {
    asm volatile ("hlt");
}

NORETURN void cpu_halt_forever() {
    while (true) {
        asm volatile ("cli; hlt");
    }
}

u64 cpu_rdtsc() {
    u32 hi, lo;
    // TODO: Verify that hi and lo are not swapped.
    asm volatile (
        "rdtsc"
        : "=edx"(hi)
        , "=eax"(lo)
    );
    return (u64)hi << 32 | (u64)lo;
}

u32 cpu_get_freq_mhz() {
    u32 result;
    asm volatile (
        "mov eax, 0x16\n"
        "cpuid"
        : "=eax"(result)
    );
    return result;
}

void cpu_loop_hint(void) {
    asm volatile ("pause");
}

// TODO: a/b, a%b, signed and unsigned 64bit
// u64 __udivdi3 (u64 a, u64 b) {
//     // ...
// }

// TODO: Don't put this here.
void sleep_milliseconds(u32 milliseconds) {
#if 0
    volatile u64 start = cpu_rdtsc();
    // TODO: freq_mhz is zero???
    volatile u32 freq_mhz = cpu_get_freq_mhz();
    // u64 time_in_cycles = (u64)milliseconds * 1000 * freq_mhz / 1000000; // __udiv3
    volatile u64 time_in_cycles = (u64)milliseconds * 1000* (freq_mhz / (1024*1024));
    volatile u64 end = start + time_in_cycles;
    while (true) {
        volatile u64 now = cpu_rdtsc();
        if (now >= end)
            break;
        cpu_loop_hint();
    }
#else
    u64 iters = (u64)milliseconds * 1000000 / 4; // good enough
    for (u64 i = 0; i < iters; i++) {
        asm volatile ("nop");
    }
#endif
}
