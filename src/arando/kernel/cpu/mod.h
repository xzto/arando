// _PUBLIC_HEADER_
#pragma once


#include <arando/corelib/basic.h>

// Should this be NORETURN?
void cpu_halt();
NORETURN void cpu_halt_forever();

u64 cpu_rdtsc();
u32 cpu_get_freq_mhz();
void cpu_loop_hint(void);

void sleep_milliseconds(u32 milliseconds); // TODO
