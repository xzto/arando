; vim: ft=nasm
[bits 32]

default rel

MAGIC_NUMBER    equ 0x1BADB002
FLAGS           equ 0x0
CHECKSUM        equ -MAGIC_NUMBER

; TODO currently, mbchk fails with -O0.
section .multiboot
align 4
    dd MAGIC_NUMBER
    dd FLAGS
    dd CHECKSUM

section .text
global _kernel_entry
_kernel_entry: ; the entry point in the linker script
    extern kernel_stack_low
    lea esp, [kernel_stack_low + KERNEL_STACK_SIZE_BYTES]
    extern kmain
    call kmain
    .x:
    cli
    hlt
    jmp .x
