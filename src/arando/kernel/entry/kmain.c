#include <arando/corelib/basic.h>
#include <arando/kernel/asm_routines/mod.h>
#include <arando/kernel/cpu/mod.h>
#include <arando/kernel/framebuffer/mod.h>
#include <arando/kernel/serial/mod.h>
#include <stdint.h>

u32 kernel_stack_low[KERNEL_STACK_SIZE_BYTES / sizeof(u32)];

static_assert(sizeof(void *) == 4);

GdtEntry gdt_entries_32[3] = {0};

const GdtDescriptor gdt_desc_32 = {
    .first_entry_linear_address = (usize)(gdt_entries_32),
    .size_in_bytes_minus_one = ARRAY_COUNT(gdt_entries_32) * sizeof(GdtEntry) - 1,
};

static void kernel_init(void) {
    serial_init();
    // serial_translate_lf_to_crlf(true); // default is true
    fb_init();

    // Refer to the intel manual chapter 3

    // the "Segment Selector" (ds, ss, etc) contains
    // | 15    - 3        | 2 | 1 - 0 |
    // | offset (index)   | ti| rpl   |
    // rpl: requested priviledge level: we want P0
    // ti: table indicator; 0 => gdt segment, 1 => ldt segment
    // the offset of the segment selector is added to the start of the gdt

    // https://wiki.osdev.org/Global_Descriptor_Table
    // https://wiki.osdev.org/GDT_Tutorial

    // this stuff is wrong
    mem_set_ptr_type_one(&gdt_entries_32[0], 0);                              // 0x00
    gdt_entry_init(&gdt_entries_32[1], 0x0, 0xffffffff, 0b10011010, 0b1000);  // 0x08
    gdt_entry_init(&gdt_entries_32[2], 0x0, 0xffffffff, 0b10010010, 0b1000);  // 0x10

#if 0
    asm volatile ("cli");
    // You load segment selectors for data registers like this.
    asm volatile (
        "mov eax, 0x10\n"
        "mov ds, eax\n" // eax is P0, in GDT, index 2 (rw)
        "mov ss, eax\n"
        "mov es, eax\n"
        "mov gs, eax\n"
        "mov fs, eax\n"
    );
    // To set the segment selector "cs" you have to do a "far jump"
    asm volatile (
        "jmp 0x08:.flush_cs\n"
        ".flush_cs:\n"
        // now cs is 0x08 (index 1 into the gdt, P0)
    );
    asm_load_gdt(&gdt_desc_32);
    asm volatile ("sti");
    // A far jump is a jump where we explicitly say the 48 bit logical address.
    // It first sets cs to 0x08 and then jumps to .flush_cs using its absolute address.
    // stuff_segment_selectors();
#endif
}

NORETURN void kmain() {
    kernel_init();

    serial_text_write(EXPAND_STR_LIT_PTR_LEN("Hello, world!!!\n"));
    serial_text_write(EXPAND_STR_LIT_PTR_LEN("Reminder: if you're on qemu, you can quit with '<c-a>x'\n"));
    // PANIC("test");
    // serial_test();
    // framebuffer_test();

    u8 c = 'a';
    i16 n_i16 = -10;
    // TODO
    TRY_OR_PANIC(writer_printf(serial_text_writer(), "Test '%d' '%d', '%x' '%X' '%c' '%c' '%s' '%.*s'\n", 123, n_i16,
                               0x1f1e8c, 0x1f1e8c, 'a', c, "Test!", EXPAND_STR_LIT_LEN_PTR("It works")));

    serial_text_write(EXPAND_STR_LIT_PTR_LEN("Kernel exit kmain no panic\n"));
    if (true) {
        PANIC("(ok) kmain end");
    } else {
        cpu_halt_forever();
    }
}
