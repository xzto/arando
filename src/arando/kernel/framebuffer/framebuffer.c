#include <arando/corelib/basic.h>
#include <arando/kernel/asm_routines/mod.h>
#include <arando/kernel/framebuffer/mod.h>
#include <arando/kernel/cpu/mod.h>

//! (x, y) starts at zero and the maximum value is (FB_SIZE_X-1, FB_SIZE_Y-1)
//! The framebuffer is 80x25 (i don't check)

static const u8 FB_BLACK = 0;
static const u8 FB_WHITE = 15;

static const u8 FB_SIZE_X = 80;
static const u8 FB_SIZE_Y = 25;

static const u16 FB_PORT_COMMAND = 0x3d4;
static const u16 FB_PORT_DATA = 0x3d5;
static const u8 FB_COMMAND_HIGH_BYTE = 0xd;
static const u8 FB_COMMAND_LOW_BYTE = 0xe;


/// `framebuffer_data` is an array of `FB_SIZE_X` * `FB_SIZE_Y` u16's. Every u16 contains `0xBFYY` where `F` is the foreground color, `B` is the background color and `YY` is the ascii character.
static volatile u16 *const framebuffer_data = ((u16*)0x000b8000);

static bool is_position_valid(u32 x, u32 y) {
    return (/* x >= 0 && */ x < FB_SIZE_X)
        && (/* y >= 0 && */ y < FB_SIZE_Y);
}

// TODO fb_init
void fb_init(void) {}

u16 fb_colorize_char(u8 ch, u8 fg, u8 bg) {
    u8 lo = ch;
    u8 hi = (u8)(((fg << 0) & 0xf) | ((bg << 4) & 0xf0));
    return (u16)(((u16)hi << 8) | ((u16)lo << 0));
}

/// Move cursor to (x, y)
void fb_move_cursor(u32 x, u32 y) {
    ASSERT(is_position_valid(x, y));
    u16 pos = (u16)(y*FB_SIZE_X + x);
    u8 pos_hi = (u8)((pos >> 8) & 0xff);
    u8 pos_lo = (u8)((pos >> 0) & 0xff);
    asm_out8(FB_PORT_COMMAND,   FB_COMMAND_HIGH_BYTE);
    asm_out8(FB_PORT_DATA,      pos_hi);
    asm_out8(FB_PORT_COMMAND,   FB_COMMAND_LOW_BYTE);
    asm_out8(FB_PORT_DATA,      pos_lo);
}

/// Put a character at (x, y)
void fb_put_char(u32 x, u32 y, u16 colorized_char) {
    if (is_position_valid(x, y)) {
        framebuffer_data[x + y*FB_SIZE_X] = colorized_char;
    } else {
        // UNIMPLEMENTED();
    }
}

/// Clears the framebuffer
void fb_clear(void) {
    const u16 c = fb_colorize_char(' ', FB_WHITE, FB_BLACK);
    for (u8 y = 0; y < FB_SIZE_Y; y++) {
        for (u8 x = 0; x < FB_SIZE_X; x++) {
            fb_put_char(x, y, c);
        }
    }
}


/// Write a string in the screen.
void fb_write(u8 const *ptr, usize len) {
    u32 x = 0; // TODO
    u32 y = 0; // TODO
    for (usize i = 0; i < len; i++) {
        u8 ch = ptr[i];
        if (ch == '\n') {
            y += 1;
            x = 0;
        } else {
            fb_put_char(x, y, fb_colorize_char(ch, FB_WHITE, FB_BLACK));
            x += 1;
        }
    }
}

void framebuffer_test() {
    fb_init();
    fb_clear();
    // fb_move_cursor(0, 0);
    const u8 n = 5;
    for (u8 y = 0; y < n; y++) {
        for (u8 x = 0; x < n; x++) {
            fb_put_char(x, y, fb_colorize_char((u8)('A' + x + y), FB_WHITE, FB_BLACK));
        }
    }
    sleep_milliseconds(5000);
    fb_write(EXPAND_STR_LIT_PTR_LEN("Hello, world!\n   Break line\n"));
    // fb_move_cursor(79, 24);
}

