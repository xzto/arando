// _PUBLIC_HEADER_
#pragma once

#include <arando/corelib/basic.h>


void framebuffer_test();

void fb_init(void);
void fb_move_cursor(u32 x, u32 y);
void fb_put_char(u32 x, u32 y, u16 colorized_char);
void fb_clear(void);
void fb_write(u8 const *ptr, usize len);
