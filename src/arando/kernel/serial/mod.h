// _PUBLIC_HEADER_
#pragma once

#include <arando/corelib/basic.h>
#include <arando/corelib/error.h>
#include <arando/kernel/writer/mod.h>
#include <stdarg.h>


void serial_test(void);

void serial_init(void);
void serial_text_put_char(u8 ch);
void serial_text_write(u8 const *ptr, usize len);
void serial_raw_put_byte(u8 ch);
void serial_raw_write(u8 const *ptr, usize len);

/// Print to serial.
__attribute__((format(printf, 1, 2))) NODISCARD ArandoError serial_printf(const char *fmt, ...);
NODISCARD ArandoError serial_printf_va(const char *fmt, va_list ap);

extern Writer const serial_text_writer_instance;
inline static NODISCARD Writer serial_text_writer(void) { return serial_text_writer_instance; }
