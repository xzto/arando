#include <arando/corelib/basic.h>
#include <arando/kernel/writer/mod.h>
#include <arando/kernel/serial/mod.h>
#include <arando/kernel/asm_routines/mod.h>
#include <arando/kernel/ticket_mutex/mod.h>
#include <arando/kernel/cpu/mod.h>


static const u16 SERIAL_COM1 = 0x3f8;

#define __r volatile const
#define __w volatile
#define _rw volatile

// This struct is only used so that I can get offsetof(Uart, x)
typedef struct {
    union {
        _rw u8 data; // DLAB 0 // read/write data buffer
        _rw u8 dll; // DLAB 1 // divisor latch low byte
    }; // 0x00
    union {
        __w u8 ier; // DLAB 0 // interrupt enable register
        _rw u8 dlh; // DLAB 1 // divisor latch high byte
    }; // 0x01
    union {
        __r u8 iir; // interrupt identification register
        __w u8 fcr; // fifo control register
    }; // 0x02
    _rw u8 lcr; // 0x03 // line control register
    _rw u8 mcr; // 0x04 // modem control register
    __r u8 lsr; // 0x05 // line status register
    __r u8 msr; // 0x06 // modem status register
    _rw u8 sr;  // 0x07 // scratch register
} UartRegs;
static_assert(sizeof(UartRegs) == 8);

#define GET_UART(com_base, it) ((com_base) + offsetof(UartRegs, it))

static const u8 LCR_ENABLE_DLAB = 0x80;
static const u8 LSR_EMPTY_TRANSMITTER_HOLDING_BIT = 0x20;

/// Setup baud rate to 115200 / `divisor` at port `com_base`.
static void setup_baud_rate(u16 com_base, u16 divisor) {
    u8 lcr = asm_in8(GET_UART(com_base, lcr));
    // Set DLAB
    asm_out8(GET_UART(com_base, lcr), lcr | LCR_ENABLE_DLAB);
    // Set divisor latch
    asm_out8(GET_UART(com_base, dll), (u8)((divisor >> 0) & 0xff));
    asm_out8(GET_UART(com_base, dlh), (u8)((divisor >> 8) & 0xff));
    // Clear DLAB
    asm_out8(GET_UART(com_base, lcr), (u8)(lcr & ~LCR_ENABLE_DLAB));
}

static void setup_line(u16 com_base) {
    // 8 bit length
    // one stop bit
    // break control disabled
    // no parity bit
    asm_out8(GET_UART(com_base, lcr), 0x03);
}

static void setup_fifo(u16 com_base) {
    asm_out8(GET_UART(com_base, fcr), 0xc7);
}

static void setup_modem(u16 com_base) {
    asm_out8(GET_UART(com_base, mcr), 0x03);
}

static bool serial_com_is_transmit_fifo_empty(u16 com_base) {
    return (asm_in8(GET_UART(com_base, lsr)) & LSR_EMPTY_TRANSMITTER_HOLDING_BIT) != 0;
}

static void serial_com_init(u16 com_base) {
    // 115200
    setup_baud_rate(com_base, 0x0001);
    setup_line(com_base);
    setup_fifo(com_base);
    setup_modem(com_base);
}

static inline void serial_com_put_char(u16 com_base, u8 ch, bool translate_lf_to_crlf) {
    // DLAB must be zero here
    while (!serial_com_is_transmit_fifo_empty(com_base)) {}
    if (translate_lf_to_crlf && ch == '\n') {
        asm_out8(GET_UART(com_base, data), '\r');
        asm_out8(GET_UART(com_base, data), '\n');
    } else {
        asm_out8(GET_UART(com_base, data), ch);
    }
}

static inline void serial_com_write(u16 com_base, u8 const *ptr, usize len, bool translate_lf_to_crlf) {
    for (usize i = 0; i < len; i++) {
        serial_com_put_char(com_base, ptr[i], translate_lf_to_crlf);
    }
}

typedef struct {
    bool did_init;
    TicketMutex mutex;
    u16 com_base;
} SerialPort;
static SerialPort global_serial;

void serial_init(void) {
    ASSERT(!global_serial.did_init);
    global_serial = (SerialPort){
        .did_init = true,
        .mutex = ticket_mutex_init(),
        .com_base = SERIAL_COM1,
    };
    serial_com_init(global_serial.com_base);
}

void serial_text_put_char(u8 ch) {
    // now that I think about it, if this assertion fails it will never print anything.
    ASSERT(global_serial.did_init);
    ticket_mutex_lock(&global_serial.mutex);
    serial_com_put_char(global_serial.com_base, ch, true);
    ticket_mutex_unlock(&global_serial.mutex);
}

void serial_text_write(u8 const *ptr, usize len) {
    ASSERT(global_serial.did_init);
    ticket_mutex_lock(&global_serial.mutex);
    serial_com_write(global_serial.com_base, ptr, len, true);
    ticket_mutex_unlock(&global_serial.mutex);
}

void serial_raw_put_byte(u8 ch) {
    ASSERT(global_serial.did_init);
    ticket_mutex_lock(&global_serial.mutex);
    serial_com_put_char(global_serial.com_base, ch, false);
    ticket_mutex_unlock(&global_serial.mutex);
}

void serial_raw_write(u8 const *ptr, usize len) {
    ASSERT(global_serial.did_init);
    ticket_mutex_lock(&global_serial.mutex);
    serial_com_write(global_serial.com_base, ptr, len, false);
    ticket_mutex_unlock(&global_serial.mutex);
}


static NODISCARD ArandoError serial_writer_write_all_fn(
    void *const user_data, u8 const *const buf_ptr, usize const buf_len
) {
    (void) user_data;
    ASSERT(global_serial.did_init);
    serial_text_write(buf_ptr, buf_len);
    return (ArandoError){ArandoError_Ok};
}

Writer const serial_text_writer_instance = {
    .user_data = NULL,
    .vtable = &(WriterVtable){
        .write_all_fn = serial_writer_write_all_fn,
    },
};


void serial_test(void) {
    serial_init();

    u64 i = 0;
    while (true) {
        serial_text_write(EXPAND_STR_LIT_PTR_LEN("Hello, world!--------------------\n"));
        // sleep_milliseconds(10);
        serial_text_write(EXPAND_STR_LIT_PTR_LEN("This is the second line!---------------\n"));
        // serial_text_write(EXPAND_STR_LIT_PTR_LEN("This is the third line!--------------------\n"));
        // serial_text_write(EXPAND_STR_LIT_PTR_LEN("This is the Fourthhh line!-------------------\n"));
        // serial_text_write(EXPAND_STR_LIT_PTR_LEN("This is the Fiffthhhhhhhh line!-------------\n"));
        // serial_text_write(EXPAND_STR_LIT_PTR_LEN("aaaaaaaaaaaaa-------------------------------\n"));
        if (i > 10) {
            serial_text_write(EXPAND_STR_LIT_PTR_LEN("=============================================================================\n"));
            i -= 10;
        }
        i += 1;
        sleep_milliseconds(400);
    }

    while (true) {}
}
