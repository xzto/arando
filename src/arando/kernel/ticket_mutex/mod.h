// _PUBLIC_HEADER_
#pragma once

#include <arando/corelib/basic.h>

typedef u32 TicketMutex_Ticket;
typedef struct {
    TicketMutex_Ticket ticket;
    TicketMutex_Ticket served;
} TicketMutex;

inline static NODISCARD TicketMutex ticket_mutex_init() {
    return ((TicketMutex){0});
}
void ticket_mutex_deinit(TicketMutex* mutex);  // nop
void ticket_mutex_lock(TicketMutex* mutex);
void ticket_mutex_unlock(TicketMutex* mutex);
NODISCARD bool ticket_mutex_try_lock(TicketMutex* mutex);
