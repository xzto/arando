#include <arando/corelib/basic.h>
#include <arando/corelib/atomic.h>
#include <arando/kernel/ticket_mutex/mod.h>
#include <arando/kernel/cpu/mod.h>


void ticket_mutex_deinit(TicketMutex *mutex) {} // nop

void ticket_mutex_lock(TicketMutex *mutex) {
    TicketMutex_Ticket ticket = atomic_fetch_add(&mutex->ticket, 1, AtomicOrder.SeqCst);
    while (atomic_load(&mutex->served, AtomicOrder.SeqCst) != ticket) {
        cpu_loop_hint();
    }
}
void ticket_mutex_unlock(TicketMutex *mutex) {
    atomic_fetch_add(&mutex->served, 1, AtomicOrder.SeqCst);
}
NODISCARD bool ticket_mutex_try_lock(TicketMutex *mutex) {
    TicketMutex_Ticket ticket = atomic_load(&mutex->ticket, AtomicOrder.SeqCst);
    if (atomic_load(&mutex->served, AtomicOrder.SeqCst) != ticket)
        return false;
    return atomic_compare_exchange(&mutex->ticket, &ticket, ticket + 1, AtomicOrder.SeqCst, AtomicOrder.SeqCst);
}
