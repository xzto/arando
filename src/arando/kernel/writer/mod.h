// _PUBLIC_HEADER_
#pragma once

#include <arando/corelib/basic.h>
#include <arando/corelib/error.h>
#include <stdarg.h>

typedef struct {
    NODISCARD ArandoError (*write_all_fn)(void *const user_data, u8 const *const buf_ptr, usize const buf_len);
} WriterVtable;

typedef struct {
    void *user_data;
    WriterVtable const *vtable;
} Writer;

/// Printf into the writer. It's equivalent to formatting into a temporary
/// buffer and then calling
/// `writer_write_all(w, fmt_result_ptr, fmt_result_len)`.
__attribute__((format(printf, 2, 3))) NODISCARD ArandoError writer_printf(Writer const w, const char *fmt, ...);
NODISCARD ArandoError writer_printf_va(Writer const w, const char *fmt, va_list ap);

inline static NODISCARD ArandoError writer_write_all(Writer const w, u8 const *const buf_ptr, usize const buf_len) {
    return w.vtable->write_all_fn(w.user_data, buf_ptr, buf_len);
}
