#include <arando/corelib/basic.h>
#include <arando/corelib/error.h>
#include <arando/kernel/writer/mod.h>

static NODISCARD ArandoError do_u32(Writer const w,
                                    u32 n,
                                    u32 const base,
                                    bool const uppercase_letters,
                                    u32 const pad_with_zeroes,
                                    usize *const out_add_count_printed) {
    u8 buf[40];
    if (base < 2 || base > 36) {
        PANIC("Invalid base");
    }
    u8 or_with_letter = uppercase_letters ? 0x00 : 0x20;
    usize i = 0;
    while (true) {
        u8 nmodbase = (u8)(n % base);
        if (nmodbase >= 10) {
            buf[i] = (u8)(nmodbase - 10 + 'A') | or_with_letter;
        } else {
            buf[i] = nmodbase + '0';
        }
        n /= base;
        i += 1;
        if (n == 0)
            break;
    }
    ASSERT(abc(i, ARRAY_COUNT(buf)));
    if (i < pad_with_zeroes) {
        while (i < pad_with_zeroes && i < ARRAY_COUNT(buf)) {
            buf[i] = '0';
            i += 1;
        }
    }
    // reverse buf[0..i]
    usize count_printed = i;
    for (i = 0; i < count_printed / 2; i++) {
        corelib_swap2(&buf[i], &buf[count_printed - i - 1]);
    }
    *out_add_count_printed += count_printed;
    return writer_write_all(w, buf, count_printed);
}

__attribute__((format(printf, 2, 3))) NODISCARD ArandoError writer_printf(Writer const w, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    ArandoError const ret = writer_printf_va(w, fmt, ap);
    va_end(ap);
    return ret;
}

NODISCARD ArandoError writer_printf_va(Writer const w, const char *fmt, va_list ap) {
    usize count_printed = 0;

#define unhandled_percent() PANIC("Unhandlde percent in print function")

    // %s // const char *cstr
    // %.*s // u32 len, const char *ptr
    // %d // i32 n
    // %u // u32 n
    // %x // u32 hex
    // %X // u32 HEX
    // %c // u8 ascii char
    typedef enum {
        e_normal,
        e_got_percent,
    } State;
    State state = e_normal;
    const u8 *s = (const u8 *)fmt;
    usize count_normal = 0;
    const u8 *first_normal = s;
    u8 c;
    while ((c = *s)) {
        switch (state) {
            case e_normal: {
                if (c == '%') {
                    if (count_normal > 0) {
                        TRY(writer_write_all(w, first_normal, count_normal));
                        count_printed += count_normal;
                        first_normal = s;
                        count_normal = 0;
                    }
                    state = e_got_percent;
                } else {
                    count_normal += 1;
                }
            } break;
            case e_got_percent: {
                switch (c) {
                    case '%': {
                        first_normal = s;
                        count_normal += 1;
                        state = e_normal;
                    } break;
                    case '.': {
                        if (s[1] == '*' && s[2] == 's') {
                            u32 len = va_arg(ap, u32);
                            const u8 *ptr = va_arg(ap, const u8 *);
                            TRY(writer_write_all(w, ptr, len));
                            count_printed += len;
                            state = e_normal;
                            first_normal = s + 3;
                            count_normal = 0;
                            s += 2;
                        } else {
                            unhandled_percent();
                        }
                    } break;
                    case 's': {
                        const u8 *cstr = va_arg(ap, const u8 *);
                        usize len = cstr_len((const char *)cstr);
                        TRY(writer_write_all(w, cstr, len));
                        count_printed += len;
                        state = e_normal;
                        first_normal = s + 1;
                        count_normal = 0;
                    } break;
                    case 'd': {
                        i32 num_ = va_arg(ap, i32);
                        bool negative = (num_ < 0);
                        u32 n;
                        if (negative) {
                            TRY(writer_write_all(w, EXPAND_STR_LIT_PTR_LEN("-")));
                            n = (u32)-num_;
                            count_printed += 1;
                        } else
                            n = (u32)num_;
                        TRY(do_u32(w, n, 10, false, 0, &count_printed));
                        first_normal = s + 1;
                        count_normal = 0;
                        state = e_normal;
                    } break;
                    case 'u': {
                        u32 n = va_arg(ap, u32);
                        TRY(do_u32(w, n, 10, false, 0, &count_printed));
                        first_normal = s + 1;
                        count_normal = 0;
                        state = e_normal;
                    } break;
                    case 'x': {
                        u32 n = va_arg(ap, u32);
                        TRY(do_u32(w, n, 16, false, 8, &count_printed));
                        first_normal = s + 1;
                        count_normal = 0;
                        state = e_normal;
                    } break;
                    case 'X': {
                        u32 n = va_arg(ap, u32);
                        TRY(do_u32(w, n, 16, true, 8, &count_printed));
                        first_normal = s + 1;
                        count_normal = 0;
                        state = e_normal;
                    } break;
                    case 'c': {
                        // ascii character
                        // TODO: Confirm that it is a u32.
                        u8 thech = (u8)va_arg(ap, u32);
                        TRY(writer_write_all(w, &thech, 1));
                        count_printed += 1;
                        first_normal = s + 1;
                        count_normal = 0;
                        state = e_normal;
                    } break;
                    default: {
                        unhandled_percent();
                    } break;
                }
            } break;
            default:
                UNREACHABLE();
                break;
        }
        s++;
    }
    if (state == e_got_percent) {
        PANIC("Printf has a % at the end.");
    }
    if (count_normal > 0) {
        TRY(writer_write_all(w, first_normal, count_normal));
        count_printed += count_normal;
        count_normal = 0;
        first_normal = 0;
    }

    return (ArandoError){ArandoError_Ok};
}
